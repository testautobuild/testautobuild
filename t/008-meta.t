# -*- perl -*-

use Test::More tests => 1;
SKIP: {
  eval "use Test::CPAN::Meta::YAML::Version; use YAML::Syck qw(LoadFile)";
  skip "Test::CPAN::Meta::YAML::Version and YAML::Syck required for testing META.yml", 1 if $@;

my %data = (
  yaml => LoadFile("META.yml"),
);

my $spec = Test::CPAN::Meta::YAML::Version->new(%data);

ok (!$spec->parse());

#warn join("\n",$spec->errors);
}
